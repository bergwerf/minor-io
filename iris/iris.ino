#include "header.h"
#include <Adafruit_NeoPixel.h>

// Compile settings
//#define IRIS_POINTER

const int beatPin = A2;
const int ratePin = A3;
const int ledCount = 29;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(ledCount, 6, NEO_GRB + NEO_KHZ800);

const int encoderPin1 = 2;
const int encoderPin2 = 3;

volatile int lastEncoded = 0;
volatile long encoderValue = 0;

int lastMSB = 0;
int lastLSB = 0;

void setup() {
  Serial.begin(9600);
  
  pinMode(beatPin, INPUT);
  pinMode(ratePin, INPUT);
  strip.begin();
  strip.show();

  pinMode(encoderPin1, INPUT);
  pinMode(encoderPin2, INPUT);

  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on     
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on

  //call updateEncoder() when any high/low changed seen
  //on interrupt 0 (pin 2), or interrupt 1 (pin 3)
  attachInterrupt(0, updateEncoder, CHANGE);
  attachInterrupt(1, updateEncoder, CHANGE);
}

void updateEncoder() {
  int MSB = digitalRead(encoderPin1); // MSB = most significant bit
  int LSB = digitalRead(encoderPin2); // LSB = least significant bit
  
  //Serial.print(MSB);
  //Serial.println(LSB);

  int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single number
  int sum = (lastEncoded << 2) | encoded; //adding it to the previous encoded value
  if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderValue++;
  if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderValue--;

  lastEncoded = encoded; //store this value for next time
} 

float global_opacity = 0.9;

// beating
float beating_opacity = 1.0;
//float breathspm = 40; // breathing: 15
//float beating_period = 60.0 / breathspm;
int beating_colors_count = 2;
RGB beating_colors[] = {
  RGB{242.0/255, 191.0/255, 17.0/255}, // orange/yellow
  RGB{242.0/255, 17.0/255, 192.0/255}  // purple/pink
};

// pointer
float pointer_offset = 0;
float pointer_width = 7;
int pointer_colors_count = 2;
RGB pointer_colors[] = {
  RGB{0, 1, 0}, // green
  RGB{0, 1, 1} // cyan
};

float pointer_fade_time = 3.0; // seconds
float pointer_fade_tstop = 0.0;
RGB pointer_color = pointer_colors[0];
int next_pointer_color = 0;

// Rate stickyness to filter noise.
float prev_rate = 0.0;

// Continuous rotating unit circle.
float theta = 0.0;
float tprev = 0.0;
float theta_leap = PI / 2;
int ring_color = 0;

void loop() {
  pointer_offset = (float)encoderValue / 1516 * ledCount;
  float rate = (float)analogRead(ratePin) / 1023.0;

  // Rate smoothing
  //rate = round(10.0 * rate) / 10.0;
  if (abs(rate - prev_rate) < 0.1) {
    rate = prev_rate;
  } else {
    prev_rate = rate;
  }
  
  float bpm = pow(1.0 + rate * 5.0, 2.0);
  float period = 60.0 / bpm;

  // breathing
  float t = (float)millis() / 1000.0;
  theta += (t - tprev) / period * 2 * PI; // one revolution in period.
  tprev = t;
  Serial.println(theta);

  if (theta - theta_leap > PI / 2) {
    ring_color++;
    theta_leap = theta + PI / 2;
  }

  float intensity = pow(sin(theta), 2);

  // beating: www.desmos.com/calculator/mpxqizyw9l
  //float fx = pow(sin(t*PI/beating_period), 100);
  //float gx = .8*pow(sin((t-.3)*PI/beating_period), 100);
  //float intensity = max(fx, gx);

  //RGB bcolor1 = beating_colors[ring_color % beating_colors_count];
  RGB bcolor1 = hsvToRgb((float)((int)abs(encoderValue) % 1000) / 1000.0, 1.0, 1.0);
  RGB bcolor2 = opacity(bcolor1, global_opacity * beating_opacity * intensity);
  setAllPixels(bcolor2);
  
  // pointer
  #ifdef IRIS_POINTER

  if (t > pointer_fade_tstop + pointer_fade_time) {
    pointer_fade_tstop = t;
    pointer_color = pointer_colors[next_pointer_color];
    if (pointer_colors_count > 1) {
      int old = next_pointer_color;
      while (next_pointer_color == old) {
        next_pointer_color = rand() % pointer_colors_count;
      }
    }
  }

  float fade_point = (t - pointer_fade_tstop) / pointer_fade_time;
  RGB pcolor1 = interpolate(pointer_color, pointer_colors[next_pointer_color], fade_point);

  for (int i = (int)floor(pointer_offset); i <= (int)ceil(pointer_offset + pointer_width); i++) {
    int j = i;
    while (j < 0) j += strip.numPixels();
    while (j >= strip.numPixels()) j -= strip.numPixels();

    float pInterpolate = pyramid(pointer_width, (float)i - pointer_offset);
    setPixel(j, opacity(interpolate(bcolor2, pcolor1, pInterpolate), global_opacity));
  }

  #endif
 
  // Update LED values.
  strip.show();
}

uint32_t rgbToInt(RGB in) {
  return strip.Color((byte)(in.r * 255), (byte)(in.g * 255), (byte)(in.b * 255));
}

void setPixel(int i, RGB color) {
  strip.setPixelColor(i, rgbToInt(color));
}

void setAllPixels(RGB color) {
  for(int i = 0; i < strip.numPixels(); i++) {
    setPixel(i, color);
  }
}

RGB opacity(RGB in, float o) {
  return RGB{o * in.r, o * in.g, o * in.b};
}

RGB interpolate(RGB a, RGB b, float biasb) {
  float biasa = 1.0 - biasb;
  return RGB{biasa*a.r+biasb*b.r, biasa*a.g+biasb*b.g, biasa*a.b+biasb*b.b};
}

RGB add(RGB a, RGB b) {
  return RGB{a.r+b.r, a.g+b.g, a.b+b.b};
}

RGB blend(RGB base, RGB top, float alpha) {
  return add(base, opacity(top, alpha));
}

RGB hsvToRgb(float h, float s, float v) {
  float i = floor(h * 6);
  float f = h * 6 - i;
  float p = v * (1 - s);
  float q = v * (1 - f * s);
  float t = v * (1 - (1 - f) * s);
  float r, g, b;
  switch ((int) i % 6) {
      case 0: r = v, g = t, b = p; break;
      case 1: r = q, g = v, b = p; break;
      case 2: r = p, g = v, b = t; break;
      case 3: r = p, g = q, b = v; break;
      case 4: r = t, g = p, b = v; break;
      case 5: r = v, g = p, b = q; break;
  }
  return RGB{r, g, b};
}

float pyramid(float w, float x) {
  // \frac{\left(-\left|x-4\right|+\frac{n}{2}\right)}{\frac{n}{2}}
  float half = w / 2;
  x -= half;
  return max(-abs(x)/half + 1, 0.0);
}

// For debugging only
void printColor(uint32_t in) {
  // GRB
  byte g = (in >> 16) & 255;
  byte r = (in >> 8) & 255;
  byte b = in & 255;
  
  Serial.print(r);
  Serial.print(" ");
  Serial.print(g);
  Serial.print(" ");
  Serial.println(b);
}
