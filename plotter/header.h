#ifndef _H
#define _H

typedef struct coord {
  float x, y;
  struct coord* next;
} Coord;

typedef struct {
  int stepPin, directionPin;
  float x, y;
  float v; // Speed in steps per second
  long pos; // Position in steps
  long target; // Target position in steps
  unsigned long lastStep; // Time of last step in microseconds
  int stepValue; // Step pin value
} StepperMotor;

#endif

